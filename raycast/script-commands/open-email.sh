#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Open Email
# @raycast.mode silent

# Optional parameters:
# @raycast.icon icons/gmail-logo.png

# Documentation:
# @raycast.description Opens gmail in Chrome
# @raycast.author Chris Toomey
# @raycast.authorURL https://ctoomey.com

~/bin/open-tab 'https://mail.google.com/mail/u/0'
