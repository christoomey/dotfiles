#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Open Trello
# @raycast.mode silent

# Optional parameters:
# @raycast.icon icons/trello-logo.png

# Documentation:
# @raycast.author Chris Toomey
# @raycast.authorURL https://ctoomey.com

~/bin/open-tab 'https://trello.com/'
