return {
  'christoomey/trusted-local',
  lazy = false,
  keys = { { '<leader>el', '<Plug>(EditVimrcLocal)' } },
  dir = '~/code/vim/trusted-local/',
}
